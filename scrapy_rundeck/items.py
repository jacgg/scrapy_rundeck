# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


#class ScrapyRundeckItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    # USER_AGENT = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:31.0) Gecko/20100101 Firefox/31.0"
#    pass



class Tienda(scrapy.Item):
    marca = scrapy.Field()
    nombre = scrapy.Field()
    descripcion = scrapy.Field()
    precio_actual = scrapy.Field()
    precio_oferta = scrapy.Field()
    descuento = scrapy.Field()
    img = scrapy.Field()
    pass