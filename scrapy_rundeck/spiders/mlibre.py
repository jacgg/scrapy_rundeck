import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.exceptions import CloseSpider

from scrapy_rundeck.items import Tienda


class MlibreSpider(CrawlSpider):
    name = 'mlibre'
    item_count = 0
    allowed_domain = ['https://listado.mercadolibre.com.pe']
    start_urls = ['https://listado.mercadolibre.com.pe/computacion/laptops/']
    rules = {
        Rule(LinkExtractor(allow=(), restrict_xpaths = ('//a[@title="Siguiente"]'))),
        Rule(LinkExtractor(allow=(),restrict_xpaths = ('//*[@id="root-app"]/div/div/section/ol//a')),
             callback='parse_item', follow=False)
    }

    def parse_item(self, response):
        tienda = Tienda()
        tienda['marca'] = response.xpath(
            'normalize-space(//*[@id="root-app"]/div/div[3]/div/div[1]/div[2]/div[2]/div/div[1]/table/tbody/tr[1]/td)').extract()
        tienda['nombre'] = response.xpath(
            'normalize-space(//*[@id="root-app"]/div/div[3]/div/div[2]/div[1]/div/div[2]/div/h1)').extract()
        tienda['descripcion'] = response.xpath(
            'normalize-space(//*[@id="root-app"]/div/div[3]/div/div[1]/div[2]/div[3]/div/p)').extract()
        tienda['precio_actual'] = response.xpath(
            'normalize-space(//*[@id="root-app"]/div/div[3]/div/div[2]/div[1]/div/div[3]/div/div/div/span/span[2])').extract()
        tienda['precio_oferta'] = response.xpath(
            'normalize-space(//*[@id="root-app"]/div/div[3]/div/div[2]/div[1]/div/div[3]/div/del/span[2])').extract()
        tienda['descuento'] = response.xpath(
            'normalize-space(//*[@id="root-app"]/div/div[3]/div/div[2]/div[1]/div/div[3]/div/div/div/span[2])').extract()
        tienda['img'] = response.xpath(
            'normalize-space(//*[@id="root-app"]/div/div[3]/div/div[1]/div[1]/div/div/div/div[2]/span[1]/figure/img/@src)').extract()

        self.item_count += 1
        if self.item_count > 7800:
            raise CloseSpider('item_exceeded')
        yield tienda
        print("*".center(100,"-"))
