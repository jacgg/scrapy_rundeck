# Web Escraping con scrapy de python

## Instalación
1. Clonar proyecto
```bash
git clone https://gitlab.com/jacgg/scrapy_rundeck.git
```
2. Creacion venv
```bash
virtualenv venv_scrapy 
```
3. activacion virtual env
```bash
source venv_scrapy/bin/activate
```

4. instalacion packages

```bash
cd scrapy_rundeck/
pip3 install -r packages.txt
```

5. iniciar scraper
```bash
scrapy crawl mlibre
```